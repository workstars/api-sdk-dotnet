﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkstarsSDK.Models;
using WorkstarsSDK.Models.Results;
using WorkstarsSDK.Services;

namespace WorkstarsUnitTest
{
    [TestClass]
    public class SdkServicesUnitTests
    {
        static string sub = "sdktestaccount" + "a";
        static string apiKey = "ff33eef7cc8ccca7567668992c6e46e5" + "";

        //instantiating the service object, using api key & subdomain
        readonly WorkstarsService _service = new WorkstarsService(sub, apiKey);

        [TestMethod]
        public void Test()
        {
            WorkstarsResult result = _service.Test();

            Console.WriteLine(result.ErrorCode.GetHashCode());
            if (!result.Success)
            {
                Assert.Fail(result.Error);
            }

            Console.WriteLine("Result Message = " + result.Message);
        }
    
    }
}
