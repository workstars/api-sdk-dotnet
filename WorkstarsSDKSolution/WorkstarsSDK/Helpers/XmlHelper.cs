﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace WorkstarsSDK.Helpers
{
    public class XmlHelper
    {
        public static void ValidateAgainstSchema(string XMLSourceDocument, XmlSchemaSet validatingSchemas)
        {
            if (validatingSchemas == null)
            {
                throw new ArgumentNullException("In ValidateAgainstSchema: No schema loaded.");
            }

            string errorHolder = string.Empty;
            ValidationHandler handler = new ValidationHandler();

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.CloseInput = true;
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationEventHandler += new ValidationEventHandler(handler.HandleValidationError);
            settings.Schemas.Add(validatingSchemas);
            settings.ValidationFlags =
                XmlSchemaValidationFlags.ReportValidationWarnings |
                XmlSchemaValidationFlags.ProcessIdentityConstraints |
                XmlSchemaValidationFlags.ProcessInlineSchema |
                XmlSchemaValidationFlags.ProcessSchemaLocation;

            StringReader srStringReader = new StringReader(XMLSourceDocument);

            using (XmlReader validatingReader = XmlReader.Create(srStringReader, settings))
            {
                while (validatingReader.Read()) { }
            }

            if (handler.MyValidationErrors.Count > 0)
            {
                foreach (String messageItem in handler.MyValidationErrors)
                {
                    errorHolder += messageItem;
                }

                throw new Exception(errorHolder);
            }
        }
    }


    public class ValidationHandler
    {
        private IList<string> myValidationErrors = new List<String>();
        public IList<string> MyValidationErrors { get { return myValidationErrors; } }

        public void HandleValidationError(object sender, ValidationEventArgs ve)
        {
            if (ve.Severity == XmlSeverityType.Error || ve.Severity == XmlSeverityType.Warning)
            {
                myValidationErrors.Add(
                    String.Format(
                    Environment.NewLine + "Line: {0}, Position {1}: \"{2}\"",
                    ve.Exception.LineNumber,
                    ve.Exception.LinePosition,
                    ve.Exception.Message)
                );
            }
        }
    }
}
