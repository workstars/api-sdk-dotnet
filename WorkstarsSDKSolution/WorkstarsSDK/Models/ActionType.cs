﻿namespace WorkstarsSDK.Models
{
    public enum ActionType
    {
        Process,
        Verify
    }
}