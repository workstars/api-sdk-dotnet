﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using RestSharp.Deserializers;

namespace WorkstarsSDK.Models.Results
{
    public class WorkstarsResult
    {
        public bool Success { get; set; }

        public string Error { get; set; }
        public int ErrorCode { get; set; }

        public string Message { get; set; }
    }

}
