﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using RestSharp.Deserializers;

namespace WorkstarsSDK.Models.Results
{
    public class EmployeeLoginResult
    {
        public bool Success { get; set; }

        public string Error { get; set; }

        public int ErrorCode { get; set; }

        [DeserializeAs(Name = "employee_id")]
        public string EmployeeId { get; set; }

        public string AccessKey { get; set; }
        public DateTime Expires { get; set; }


        public override string ToString()
        {
            return
                $"Success: {Success}, " +
                $"Error: {Error}, " +
                $"EmployeeId: {EmployeeId}, " +
                $"AccessKey: {AccessKey}, " +
                $"Expires: {Expires}";

        }
    }

}
