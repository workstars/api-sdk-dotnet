﻿using System;

namespace WorkstarsSDK.Models.Requests
{
    public class FeedRequest
    {
        public ActionType Action { get; set; }

        public string FilePath { get; set; }
    }
}
