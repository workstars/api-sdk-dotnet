﻿using RestSharp;
using RestSharp.Authenticators;
using System.Net;
using WorkstarsSDK.Resources;

namespace WorkstarsSDK.Services.HttpClient
{
    /// <summary>
    /// Implementing our own RestClient
    /// RestClient is the RestSharp HttpClient
    /// </summary>
    public class WorkstarsRestClient : RestClient
    {
        //The host url
        private static string HostUrl = "https://{0}.workstars.com";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="subDomain"></param>
        /// <param name="apiKey"></param>
        public WorkstarsRestClient(string subDomain, string apiKey, string clientAppHeader = null)
            : base(string.Format(HostUrl, subDomain))
        {

            // Updating the used SecurityProtocol
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            this.FollowRedirects = false;

            //Authentificate the HttpClient using the apiKey
            this.Authenticator = new HttpBasicAuthenticator("api-key", apiKey);

            //Adding the sdk version to the Http Header
            this.AddDefaultHeader(WorkstarsResource.SdkHttpHeaderKey, WorkstarsResource.SdkHttpHeaderValue);

            if (!string.IsNullOrWhiteSpace(clientAppHeader))
            {
                //Adding the app version  to the Http Header
                this.AddDefaultHeader("app", clientAppHeader);
            }

            //Default timeout value = 3 sec
            Timeout = 3000;
        }

    }
}
