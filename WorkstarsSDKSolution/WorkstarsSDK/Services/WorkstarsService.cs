﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using WorkstarsSDK.Helpers;
using WorkstarsSDK.Models;
using WorkstarsSDK.Models.Requests;
using WorkstarsSDK.Models.Results;
using WorkstarsSDK.Resources;
using WorkstarsSDK.Services.HttpClient;

namespace WorkstarsSDK.Services
{
    public partial class WorkstarsService
    {
        public string ApiKey { get; set; }
        public string SubDomain { get; set; }

        public string ClientAppHeader { get; set; }

        public WorkstarsService(string subDomain, string apiKey)
        {
            ApiKey = apiKey;
            SubDomain = subDomain;
        }

        /// <summary>
        /// Test Service function
        /// </summary>
        /// <returns></returns>
        public WorkstarsResult Test()
        {
            //Ressource for this service
            var ressource = "admin/api/v2/test";

            //Creating the Workstars Rest Client object
            var client = new WorkstarsRestClient(SubDomain, ApiKey, this.ClientAppHeader);

            // 
            var request = new RestRequest(ressource, Method.GET);

            var response = client.Execute<WorkstarsResult>(request);

            var data = response?.Data;

            if (response != null)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (data == null)
                    {
                        data = new WorkstarsResult
                        {
                            Error = WorkstarsResource.ParsingError,
                            ErrorCode = 0
                        };
                    }
                    else
                    {
                        data.Success = true;
                    }
                }
                else
                {
                    if (data == null)
                    {
                        data = new WorkstarsResult();
                    }

                    data.Error = response.Content;
                }

                data.ErrorCode = response.StatusCode.GetHashCode();

            }
            else
            {
                data = new WorkstarsResult
                {
                    Error = WorkstarsResource.UnreachableServer,
                    ErrorCode = 0
                };
            }

            return data;
        }

        private WorkstarsResult Employee(FeedRequest employeeFeed)
        {
            // Validate File Schema before proceeding 
            var validationResult = ValidateEmployeeFileSchema(employeeFeed.FilePath);

            if (!validationResult.Success)
            {
                return validationResult;
            }

            //Ressource for this service
            var ressource = "admin/api/v2/hrdatasync/employee";

            //Creating the client
            var client = new WorkstarsRestClient(SubDomain, ApiKey, this.ClientAppHeader);

            var request = new RestRequest(ressource, Method.POST);

            try
            {
                var fileInfo = new FileInfo(employeeFeed.FilePath);

                request.AddFile("file",
                    File.ReadAllBytes(employeeFeed.FilePath),
                    Path.GetFileName(employeeFeed.FilePath),
                    "application/octet-stream");
            }
            catch (Exception)
            {
                return new WorkstarsResult
                {
                    Error = WorkstarsResource.FileError,
                    Success = false,
                    Message = null,
                    ErrorCode = 0
                };
            }

            var response = client.Execute<WorkstarsResult>(request);

            var data = response?.Data;

            if (response != null)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (data == null)
                    {
                        data = new WorkstarsResult
                        {
                            Error = WorkstarsResource.ParsingError,
                            ErrorCode = 0
                        };
                    }
                    else
                    {
                        data.Success = true;
                    }
                }
                else
                {
                    if (data == null)
                    {
                        data = new WorkstarsResult();
                    }

                    data.Error = response.Content;
                }

                data.ErrorCode = response.StatusCode.GetHashCode();

            }
            else
            {
                data = new WorkstarsResult
                {
                    Error = WorkstarsResource.UnreachableServer,
                    ErrorCode = 0
                };
            }

            return data;
        }

        private WorkstarsResult Employee(string filePath, ActionType action)
        {
            return Employee(new FeedRequest
            {
                FilePath = filePath,
                Action = action
            });
        }

        public WorkstarsResult ValidateEmployeeFileSchema(string filePath)
        {
            //Ressource for this service
            var xmlRessource = "/admin/api/v2/hrdatasync/employees.xsd";

            return ValidateFileSchema(filePath, xmlRessource);
        }

        public WorkstarsResult ValidatePositionFileSchema(string filePath)
        {
            //Ressource for this service
            var xmlRessource = "/admin/api/v2/hrdatasync/positions.xsd";

            return ValidateFileSchema(filePath, xmlRessource);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="xsdRessource">Example : "/admin/api/v2/hrdatasync/employees.xsd"</param>
        /// <returns></returns>
        public WorkstarsResult ValidateFileSchema(string filePath, string xsdRessource)
        {
            //Creating the client
            var client = new WorkstarsRestClient(SubDomain, ApiKey, this.ClientAppHeader);

            var request = new RestRequest(xsdRessource, Method.GET);

            var response = client.Execute(request);

            // Get the content of the xsd File
            var xsdContent = response?.Content;

            var result = new WorkstarsResult();

            if (response != null)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (string.IsNullOrEmpty(xsdContent))
                    {
                        result.Error = WorkstarsResource.ParsingError;
                        result.ErrorCode = 0;
                    }
                    else
                    {
                        XmlReader xmlReader = XmlReader.Create(new StringReader(xsdContent));

                        XmlDocument xml = new XmlDocument();

                        // Create the XmlSchemaSet class.
                        XmlSchemaSet sc = new XmlSchemaSet();

                        // Add the schema to the collection.
                        sc.Add(null, xmlReader);

                        string xmlFileContent = string.Empty;

                        try
                        {
                            xmlFileContent = File.ReadAllText(filePath);
                        }
                        catch (Exception ex)
                        {
                            return new WorkstarsResult
                            {
                                Error = WorkstarsResource.FileError,
                                Success = false,
                                Message = null,
                                ErrorCode = 0
                            };
                        }

                        try
                        {
                            XmlHelper.ValidateAgainstSchema(xmlFileContent, sc);

                            // Validation Success
                            result.Success = true;
                        }
                        catch (Exception ex)
                        {
                            result.Success = false;
                            result.Error = ex.Message;
                            result.Message = null;
                            result.ErrorCode = 0;
                        }

                        return result;
                    }
                }
                else
                {
                    if (result == null)
                    {
                        result = new WorkstarsResult();
                    }

                    result.Error = response.Content;
                }

                result.ErrorCode = response.StatusCode.GetHashCode();
            }
            else
            {
                result = new WorkstarsResult
                {
                    Error = WorkstarsResource.UnreachableServer,
                    ErrorCode = 0
                };
            }

            return result;
        }


        private WorkstarsResult Position(FeedRequest positionFeed)
        {
            // Validate File Schema before proceeding 
            var validationResult = ValidatePositionFileSchema(positionFeed.FilePath);

            if (!validationResult.Success)
            {
                return validationResult;
            }

            //Ressource for this service
            var ressource = "admin/api/v2/hrdatasync/position";

            //Creating the client
            var client = new WorkstarsRestClient(SubDomain, ApiKey, this.ClientAppHeader);

            var request = new RestRequest(ressource, Method.POST);

            try
            {
                var fileInfo = new FileInfo(positionFeed.FilePath);

                request.AddFile("file",
                    File.ReadAllBytes(positionFeed.FilePath),
                    Path.GetFileName(positionFeed.FilePath),
                    "application/octet-stream");
            }
            catch (Exception)
            {
                return new WorkstarsResult
                {
                    Error = WorkstarsResource.FileError,
                    Success = false,
                    Message = null,
                    ErrorCode = 0
                };
            }

            var response = client.Execute<WorkstarsResult>(request);

            var data = response?.Data;

            if (response != null)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (data == null)
                    {
                        data = new WorkstarsResult
                        {
                            Error = WorkstarsResource.ParsingError,
                            ErrorCode = 0
                        };
                    }
                    else
                    {
                        data.Success = true;
                    }
                }
                else
                {
                    if (data == null)
                    {
                        data = new WorkstarsResult();
                    }

                    data.Error = response.Content;
                }

                data.ErrorCode = response.StatusCode.GetHashCode();

            }
            else
            {
                data = new WorkstarsResult
                {
                    Error = WorkstarsResource.UnreachableServer,
                    ErrorCode = 0
                };
            }

            return data;

        }


        private WorkstarsResult Position(string filePath, ActionType action)
        {
            return Position(new FeedRequest
            {
                Action = action,
                FilePath = filePath
            });
        }

        public WorkstarsResult UploadEmployeeFile(string filePath)
        {
            return Employee(filePath, ActionType.Process);
        }

        public WorkstarsResult UploadPositionFile(string filePath)
        {
            return Position(filePath, ActionType.Process);
        }

    }

}