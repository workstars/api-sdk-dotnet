﻿using System;
using WorkstarsSDK.Models;
using WorkstarsSDK.Models.Results;
using WorkstarsSDK.Services;

namespace WorkstarsSDKConsoleTests
{
    public class Program
    {
        static void Main(string[] args)
        {
            string subDomain = "sdk-test-demo";
            string apiKey = "pehsqy3368dgybhowu1tm3qe57oxraw1";

            //instantiating the service object, using api key & subdomain
            WorkstarsService service = new WorkstarsService(subDomain, apiKey);
            service.ClientAppHeader = "Test Client APP 2";


            /*** Test service availability ***/
            WorkstarsResult test = service.Test();

            if (test.Success)
            {
                Console.WriteLine(test.Message);
            }
            else
            {
                Console.WriteLine(test.Error);
            }
            /*** End Test service ***/

            /*** EmployeeXSD ***/
            WorkstarsResult validateEmployeeXmlFileResult = service.ValidateEmployeeFileSchema(@"C:\test\default_employees.xml");
            if (validateEmployeeXmlFileResult.Success)
            {
                Console.WriteLine("Validation Success");
                Console.WriteLine(validateEmployeeXmlFileResult.Message);
            }
            else
            {
                Console.WriteLine(validateEmployeeXmlFileResult.Error);
            }
            /*** End EmployeeXSD ***/

            
            /*** Employee service ***/
            string filePath = @"C:\test\default_employees.xml";


            WorkstarsResult result = service.UploadEmployeeFile(filePath);
            // Or : WorkstarsResult result = service.TestFeedFile(TestFeedType.Employee, filePath);

            if (result.Success)
            {
                Console.WriteLine(test.Message);
            }
            else
            {
                Console.WriteLine(result.Error);
            }
            /*** End Employee service ***/

            Console.WriteLine(" * END * ");
            Console.Read();

        }
    }
}
